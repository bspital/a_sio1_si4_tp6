package controleurs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class control2 {
    
    float salaire;
    int nbenfant;
    int nbannee;
    float primebase;
    float primeenfant;
    float primeanciennete;
    float prime;
    
    
    public void calcul() {
        if(salaire<=1600) { setPrimebase(180); }
        if(salaire>1600&&salaire<=2200) { setPrimebase(150); }
        if(salaire>2200&&salaire<=3000) { setPrimebase(100); }
        if(salaire>3000) { setPrimebase(60); }
        
        setPrimeenfant(nbenfant*110);
        
        if(nbannee>=1) { setPrimeanciennete(90); }
        if(nbannee>=5) { setPrimeanciennete(120); }
        if(nbannee>=10) { setPrimeanciennete(150); }
        
        setPrime(primebase+primeenfant+primeanciennete);
    }
    
    
    
    public static final String PROP_PRIME = "prime";

    public float getPrime() {
        return prime;
    }

    public void setPrime(float prime) {
        float oldPrime = this.prime;
        this.prime = prime;
        propertyChangeSupport.firePropertyChange(PROP_PRIME, oldPrime, prime);
    }

    public static final String PROP_PRIMEANCIENNETE = "primeanciennete";

    public float getPrimeanciennete() {
        return primeanciennete;
    }

    public void setPrimeanciennete(float primeanciennete) {
        float oldPrimeanciennete = this.primeanciennete;
        this.primeanciennete = primeanciennete;
        propertyChangeSupport.firePropertyChange(PROP_PRIMEANCIENNETE, oldPrimeanciennete, primeanciennete);
   }
    
    public static final String PROP_PRIMEENFANT = "primeenfant";

    public float getPrimeenfant() {
        return primeenfant;
    }

    public void setPrimeenfant(float primeenfant) {
        float oldPrimeenfant = this.primeenfant;
        this.primeenfant = primeenfant;
        propertyChangeSupport.firePropertyChange(PROP_PRIMEENFANT, oldPrimeenfant, primeenfant);
    }

    
    public static final String PROP_PRIMEBASE = "primebase";

    public float getPrimebase() {
        return primebase;
    }

    public void setPrimebase(float primebase) {
        float oldPrimebase = this.primebase;
        this.primebase = primebase;
        propertyChangeSupport.firePropertyChange(PROP_PRIMEBASE, oldPrimebase, primebase);
    }

    public static final String PROP_NBANNEE = "nbannee";

    public int getNbannee() {
        return nbannee;
    }

    public void setNbannee(int nbannee) {
        int oldNbannee = this.nbannee;
        this.nbannee = nbannee;
        propertyChangeSupport.firePropertyChange(PROP_NBANNEE, oldNbannee, nbannee);
    }

    public static final String PROP_NBENFANT = "nbenfant";

    public int getNbenfant() {
        return nbenfant;
    }

    public void setNbenfant(int nbenfant) {
        int oldNbenfant = this.nbenfant;
        this.nbenfant = nbenfant;
        propertyChangeSupport.firePropertyChange(PROP_NBENFANT, oldNbenfant, nbenfant);
    }
 
    public static final String PROP_SALAIRE = "salaire";

    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        float oldSalaire = this.salaire;
        this.salaire = salaire;
        propertyChangeSupport.firePropertyChange(PROP_SALAIRE, oldSalaire, salaire);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

}

package controleurs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class control {
    
    int age;
    int inscription;
    int total;
    boolean troisfois = false;
    String payement;

    public void calcul() {
        if(age<=8) { setInscription(45); }
        if(age>8 && age<=12) { setInscription(60); }
        if(age>12 && age<=15) { setInscription(75); }
        if(age>15) { setInscription(90); }
        
        setTotal(inscription+80);
    }
    
    public void payement() {
        if(troisfois) {
            setPayement("Vous payerez en 3x, le 15 septembre,\n 15 janvier et 15 avril la somme de: "+(float) total/3+"€");
        }
        else {
            setPayement("Vous payerez en une fois\n le 15 septembre la somme de "+total+"€");
        }
    }
    
    
    
    
    
    
    
    public static final String PROP_TROISFOIS = "troisfois";

    public boolean isTroisfois() {
        return troisfois;
    }

    public void setTroisfois(boolean troisfois) {
        boolean oldTroisfois = this.troisfois;
        this.troisfois = troisfois;
        propertyChangeSupport.firePropertyChange(PROP_TROISFOIS, oldTroisfois, troisfois);
    }

    public static final String PROP_PAYEMENT = "payement";

    public String getPayement() {
        return payement;
    }

    public void setPayement(String payement) {
        String oldPayement = this.payement;
        this.payement = payement;
        propertyChangeSupport.firePropertyChange(PROP_PAYEMENT, oldPayement, payement);
    }

    public static final String PROP_TOTAL = "total";

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        int oldTotal = this.total;
        this.total = total;
        propertyChangeSupport.firePropertyChange(PROP_TOTAL, oldTotal, total);
    }

    public static final String PROP_INSCRIPTION = "inscription";

    public int getInscription() {
        return inscription;
    }

    public void setInscription(int inscription) {
        int oldInscription = this.inscription;
        this.inscription = inscription;
        propertyChangeSupport.firePropertyChange(PROP_INSCRIPTION, oldInscription, inscription);
    }

    public static final String PROP_AGE = "age";

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        int oldAge = this.age;
        this.age = age;
        propertyChangeSupport.firePropertyChange(PROP_AGE, oldAge, age);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

}
